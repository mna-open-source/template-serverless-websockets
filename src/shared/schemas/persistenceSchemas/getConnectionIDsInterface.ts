import { CustomConnectionData } from '../../entities/customConnectionData';

export interface GetConnectionIDsInterface {
  (): Promise<CustomConnectionData[]>;
}
