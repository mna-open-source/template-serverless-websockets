import { JSONSchemaType } from 'ajv';
import { DisconnectionData } from '../../entities/disconnectionData';

export const disconnectionSchema: JSONSchemaType<DisconnectionData> = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    payload: { type: 'null', nullable: true },
  },
  required: ['id'],
};
