import { JSONSchemaType } from 'ajv';
import { Payload } from '../../entities/payload';

export const payloadSchema: JSONSchemaType<Payload> = {
  type: 'object',
  properties: {
    type: { type: 'string' },
    data: { type: 'object' },
  },
  required: ['type', 'data'],
};
