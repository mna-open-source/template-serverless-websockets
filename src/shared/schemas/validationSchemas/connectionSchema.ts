import { JSONSchemaType } from 'ajv';
import { CustomConnectionData } from '../../entities/customConnectionData';

export const connectionSchema: JSONSchemaType<CustomConnectionData> = {
  type: 'object',
  properties: {
    id: { type: 'string' },
    payload: {
      type: 'object',
      properties: {
        requestId: { type: 'string' },
        otherId: { type: 'string' },
      },
      required: ['requestId', 'otherId'],
    },
  },
  required: ['id', 'payload'],
};
