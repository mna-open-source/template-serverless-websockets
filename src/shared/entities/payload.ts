/* eslint-disable @typescript-eslint/ban-types */
export class Payload {
  type: string;

  data: object;
}
