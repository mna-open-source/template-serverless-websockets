import { ConnectionData } from 'src/framework/contracts/connectionData';

export class DisconnectionData extends ConnectionData<null> {}
