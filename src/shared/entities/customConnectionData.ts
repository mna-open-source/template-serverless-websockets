import { ConnectionData } from 'src/framework/contracts/connectionData';

export class CustomConnectionData extends ConnectionData<{ requestId: string; otherId: string }> {}
