import { LoggerScope, MessageRequest, UseCaseInterface } from 'src/framework/contracts';
import { Logger } from 'src/framework/logger';
import { Payload } from '../entities/payload';
import { GetConnectionIDsInterface } from '../schemas/persistenceSchemas/getConnectionIDsInterface';

const useCaseName = 'sendMessage';

/**
 * Use case.
 */
export const sendMessage =
  (getConnectionIDs: GetConnectionIDsInterface): UseCaseInterface<Payload, MessageRequest<Payload>[]> =>
  async (body: Payload): Promise<MessageRequest<Payload>[]> => {
    const logger = new Logger(useCaseName, LoggerScope.UseCase);
    logger.info({ message: `The use case function '${useCaseName}' was invoked`, body });

    const connections = await getConnectionIDs();

    logger.info({ connections });

    const requests: MessageRequest<Payload>[] = [];

    if (connections) {
      connections.forEach(({ id }) => {
        requests.push({
          connectionId: id,
          body,
        });
      });
    }

    return requests;
  };
