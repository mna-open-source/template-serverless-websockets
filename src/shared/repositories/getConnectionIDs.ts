import { CacheHandler, ConnectionsDB, getConnection, RedisHandler } from 'src/framework/persistence';
import { CustomConnectionData } from 'src/shared/entities/customConnectionData';
import { GetConnectionIDsInterface } from '../schemas/persistenceSchemas/getConnectionIDsInterface';

export const getConnectionIDs: GetConnectionIDsInterface = () => {
  const { client, prefix } = getConnection();
  const cacheHandler: CacheHandler = new RedisHandler(client);
  return new ConnectionsDB(CustomConnectionData, cacheHandler, prefix).findAll();
};
