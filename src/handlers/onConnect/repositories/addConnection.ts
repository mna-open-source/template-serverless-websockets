import { CacheHandler, ConnectionsDB, getConnection, RedisHandler } from 'src/framework/persistence';
import { CustomConnectionData } from 'src/shared/entities/customConnectionData';
import { AddConnectionInterface } from '../schemas/persistenceSchemas/addConnectionInterface';

export const addConnection: AddConnectionInterface = (connectionData: CustomConnectionData) => {
  const { client, prefix } = getConnection();
  const cacheHandler: CacheHandler = new RedisHandler(client);
  return new ConnectionsDB(CustomConnectionData, cacheHandler, prefix).saveOne(connectionData.payload.requestId, connectionData);
};
