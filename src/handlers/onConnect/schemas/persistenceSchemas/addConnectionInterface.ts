import { CustomConnectionData } from 'src/shared/entities/customConnectionData';

export interface AddConnectionInterface {
  (connectionData: CustomConnectionData): Promise<any>;
}
