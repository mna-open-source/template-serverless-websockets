import { LoggerScope, UseCaseInterface } from 'src/framework/contracts';
import { Logger } from 'src/framework/logger';
import { CustomConnectionData } from 'src/shared/entities/customConnectionData';
import { AddConnectionInterface } from '../schemas/persistenceSchemas/addConnectionInterface';

const useCaseName = 'onConnect';

/**
 * Use case.
 */
export const onConnect =
  (addConnection: AddConnectionInterface): UseCaseInterface<CustomConnectionData, any> =>
  (connectionData: CustomConnectionData): Promise<any> => {
    const logger = new Logger(useCaseName, LoggerScope.UseCase);
    logger.info({ message: `The use case function '${useCaseName}' was invoked`, connectionData });
    return addConnection(connectionData);
  };
