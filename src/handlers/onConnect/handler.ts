import { BodyValidatorInterface, UseCaseInterface } from 'src/framework/contracts';
import { wsConnectionsHandler } from 'src/framework/controllers/webSocket';
import { validateSchema } from 'src/framework/libs/validateSchema';
import { bodyParser } from 'src/framework/libs/bodyParser';
import { customExceptionsMatcher } from 'src/shared/customExceptions';
import { CustomConnectionData } from 'src/shared/entities/customConnectionData';
import { connectionSchema } from 'src/shared/schemas/validationSchemas/connectionSchema';
import { onConnect } from './useCases/onConnect';
import { addConnection } from './repositories/addConnection';

const useCase: UseCaseInterface<CustomConnectionData, any> = onConnect(addConnection);
const customBodyValidator: BodyValidatorInterface = validateSchema(connectionSchema);
const parseBody = bodyParser(CustomConnectionData, customBodyValidator);

export const main = wsConnectionsHandler(parseBody, useCase, customExceptionsMatcher);
