import { LoggerScope, UseCaseInterface } from 'src/framework/contracts';
import { Logger } from 'src/framework/logger';
import { DisconnectionData } from 'src/shared/entities/disconnectionData';
import { DeleteConnectionInterface } from '../schemas/persistenceSchemas/deleteConnectionInterface';

const useCaseName = 'onDisconnect';

/**
 * Use case.
 */
export const onDisconnect =
  (deleteConnection: DeleteConnectionInterface): UseCaseInterface<DisconnectionData, any> =>
  (disconnectionData: DisconnectionData): Promise<any> => {
    const logger = new Logger(useCaseName, LoggerScope.UseCase);
    logger.info({ message: `The use case function '${useCaseName}' was invoked`, disconnectionData });
    return deleteConnection(disconnectionData);
  };
