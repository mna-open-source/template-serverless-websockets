import { BodyValidatorInterface, UseCaseInterface } from 'src/framework/contracts';
import { wsConnectionsHandler } from 'src/framework/controllers/webSocket';
import { validateSchema } from 'src/framework/libs/validateSchema';
import { bodyParser } from 'src/framework/libs/bodyParser';
import { customExceptionsMatcher } from 'src/shared/customExceptions';
import { DisconnectionData } from 'src/shared/entities/disconnectionData';
import { disconnectionSchema } from 'src/shared/schemas/validationSchemas/disconnectionSchema';
import { onDisconnect } from './useCases/onDisconnect';
import { deleteConnection } from './repositories/deleteConnection';

const useCase: UseCaseInterface<DisconnectionData, any> = onDisconnect(deleteConnection);
const customBodyValidator: BodyValidatorInterface = validateSchema(disconnectionSchema);
const parseBody = bodyParser(DisconnectionData, customBodyValidator);

export const main = wsConnectionsHandler(parseBody, useCase, customExceptionsMatcher);
