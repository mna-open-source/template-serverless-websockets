import { handlerPath } from 'src/framework/libs/handlerResolver';

export default {
  handler: `${handlerPath(__dirname)}/handler.main`,
  events: [
    {
      websocket: {
        route: '$disconnect',
      },
    },
  ],
};
