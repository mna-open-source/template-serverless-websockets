import { CacheHandler, ConnectionsDB, getConnection, RedisHandler } from 'src/framework/persistence';
import { DisconnectionData } from 'src/shared/entities/disconnectionData';
import { DeleteConnectionInterface } from '../schemas/persistenceSchemas/deleteConnectionInterface';

export const deleteConnection: DeleteConnectionInterface = ({ id }: DisconnectionData) => {
  const { client, prefix } = getConnection();
  const cacheHandler: CacheHandler = new RedisHandler(client);
  return new ConnectionsDB(DisconnectionData, cacheHandler, prefix).deleteAll('*', id);
};
