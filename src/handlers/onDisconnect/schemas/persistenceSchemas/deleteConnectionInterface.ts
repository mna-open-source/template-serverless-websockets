import { DisconnectionData } from 'src/shared/entities/disconnectionData';

export interface DeleteConnectionInterface {
  (disconnectionData: DisconnectionData): Promise<any>;
}
