import { BodyValidatorInterface, MessageRequest, UseCaseInterface } from 'src/framework/contracts';
import { webSocketHandler } from 'src/framework/controllers/webSocket';
import { validateSchema } from 'src/framework/libs/validateSchema';
import { bodyParser } from 'src/framework/libs/bodyParser';
import { customExceptionsMatcher } from 'src/shared/customExceptions';
import { sendMessage } from 'src/shared/useCases/sendMessage';
import { getConnectionIDs } from 'src/shared/repositories/getConnectionIDs';
import { payloadSchema } from 'src/shared/schemas/validationSchemas/payloadSchema';
import { Payload } from 'src/shared/entities/payload';

const useCase: UseCaseInterface<Payload, MessageRequest<Payload>[]> = sendMessage(getConnectionIDs);
const customBodyValidator: BodyValidatorInterface = validateSchema(payloadSchema);
const parseBody = bodyParser(Payload, customBodyValidator);

export const main = webSocketHandler(parseBody, useCase, customExceptionsMatcher);
