import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult, Context } from 'aws-lambda';
import { StatusCodes } from 'http-status-codes';
import { InternalServerException } from 'src/framework/exceptions/presentation';
import { LocaleService } from 'src/framework/locale/localeService';
import { WebSocketPresenter } from 'src/framework/presenters/webSocket/webSocketPresenter';
import { postMultiple } from 'src/framework/libs/postMultiple';
import { Logger } from 'src/framework/logger';
import {
  UseCaseInterface,
  ExceptionsMatcherInterface,
  BodyParserInterface,
  ConnectionData,
  MessageRequest,
  LoggerScope,
  LoggerSource,
  BusinessExceptions,
  BaseHttpException,
} from '../contracts';
import { exceptionsMatcher } from '../exceptions/exceptionsMatcher';

const start = (event: APIGatewayProxyEvent, context: Context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  new LocaleService().setLocale(event.headers !== undefined ? event.headers['Accept-Language'] || 'en-US' : 'en-US');

  const logger = new Logger(context.functionName, LoggerScope.Handler, LoggerSource.ApiGateway);
  logger.info({ message: `The AWS lambda function '${context.functionName}' was invoked`, event });
};

const handleError = (em: ExceptionsMatcherInterface = exceptionsMatcher, error: any): APIGatewayProxyResult => {
  const presenter = new WebSocketPresenter();
  if (error instanceof BusinessExceptions) return presenter.error(em(error));
  if (error instanceof BaseHttpException) return presenter.error(error);
  return presenter.error(new InternalServerException(error));
};

export const wsConnectionsHandler =
  <T>(
    parseBody: BodyParserInterface<ConnectionData<T>>,
    useCase: UseCaseInterface<ConnectionData<T>, any>,
    em: ExceptionsMatcherInterface = exceptionsMatcher,
  ): APIGatewayProxyHandler =>
  async (event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> => {
    start(event, context);

    const rawBody = {
      id: event.requestContext.connectionId,
      payload: event.queryStringParameters,
    };

    try {
      const parsed = parseBody(rawBody);
      const result = await useCase(parsed);
      return new WebSocketPresenter().success(result, StatusCodes.OK);
    } catch (error) {
      return handleError(em, error);
    }
  };

export const webSocketHandler =
  <T>(
    parseBody: BodyParserInterface<T>,
    useCase: UseCaseInterface<T, MessageRequest<T>[]>,
    em: ExceptionsMatcherInterface = exceptionsMatcher,
  ): APIGatewayProxyHandler =>
  async (event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> => {
    start(event, context);

    const rawBody = event.body !== undefined ? event.body : event;

    try {
      const parsed = parseBody(rawBody);
      const requests: MessageRequest<T>[] = await useCase(parsed);
      const result = await postMultiple(event, requests);
      return new WebSocketPresenter().success(result, StatusCodes.OK);
    } catch (error) {
      return handleError(em, error);
    }
  };
