export class MessageRequest<T> {
  connectionId: string;

  body: T;
}
