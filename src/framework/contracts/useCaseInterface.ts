export interface UseCaseInterface<T, R> {
  (body: T): Promise<R>;
}
