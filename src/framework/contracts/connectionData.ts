export class ConnectionData<T> {
  id: string;

  payload: T;
}
