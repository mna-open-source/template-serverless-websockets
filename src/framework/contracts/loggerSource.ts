export enum LoggerSource {
  SQS = 'sqs',
  ApiGateway = 'api-gateway',
  WebSocket = 'web-socket',
}
