import { plainToClass } from 'class-transformer';
import { ClassConstructor, ConnectionData } from '../contracts';
import { CacheHandler } from './cacheHandler';

export class ConnectionsDB<T> {
  private readonly classConstructor: ClassConstructor<ConnectionData<T>>;

  private readonly prefix: string;

  private readonly cacheHandler: CacheHandler;

  constructor(classConstructor: ClassConstructor<ConnectionData<T>>, cacheHandler: CacheHandler, prefix: string) {
    this.classConstructor = classConstructor;
    this.prefix = prefix;
    this.cacheHandler = cacheHandler;
  }

  private composeKey(requestId: string, connectionId: string): string {
    return this.prefix.concat(requestId, ':connectionId:', connectionId);
  }

  findOne(requestId: string, connectionId: string): Promise<string | null> {
    return this.cacheHandler.getValue(this.composeKey(requestId, connectionId));
  }

  async findAll(requestId: string = '*', connectionId: string = '*'): Promise<ConnectionData<T>[]> {
    const pattern: string = this.composeKey(requestId, connectionId);
    const specificKeys: string[] = await this.cacheHandler.findKeys(pattern);
    const connectionsData: string[] = await this.cacheHandler.getValues(specificKeys);
    return connectionsData.map((stringConnectionData) => {
      const rawConnectionData = JSON.parse(stringConnectionData);
      return plainToClass(this.classConstructor, rawConnectionData);
    });
  }

  saveOne(requestId: string, value: ConnectionData<T>): Promise<unknown> {
    return this.cacheHandler.set(this.composeKey(requestId, value.id), JSON.stringify(value));
  }

  deleteOne(requestId: string, connectionId: string): Promise<number> {
    return this.cacheHandler.deleteKey(this.composeKey(requestId, connectionId));
  }

  async deleteAll(requestId: string = '*', connectionId: string = '*'): Promise<void> {
    const pattern: string = this.composeKey(requestId, connectionId);
    const specificKeys: string[] = await this.cacheHandler.findKeys(pattern);

    const promises = [];

    for (const key of specificKeys) {
      promises.push(this.cacheHandler.deleteKey(key));
    }

    await Promise.all(promises);
  }
}
