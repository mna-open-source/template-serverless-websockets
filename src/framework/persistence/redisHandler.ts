import { createClient, RedisClient } from 'redis';
import { promisify } from 'util';
import { CacheHandler } from './cacheHandler';
import { NonexistentEnvVarException } from '../exceptions/business';

let client: RedisClient;
let prefix: string | undefined;

export const getConnection = (): { client: RedisClient; prefix: string } => {
  if (client && prefix) {
    return {
      client,
      prefix,
    };
  }

  prefix = process.env.WS_CONNECTIONS_KEY_PREFIX;

  if (!prefix) {
    throw new NonexistentEnvVarException("Unknown 'WS_CONNECTIONS_KEY_PREFIX'");
  }

  const port: number = parseInt(process.env.WS_CONNECTIONS_PORT || '8888', 10);
  const host: string | undefined = process.env.WS_CONNECTIONS_HOST;
  const password: string | undefined = process.env.WS_CONNECTIONS_PASSWORD;

  client = createClient({ port, host, password });

  return {
    client,
    prefix,
  };
};

export class RedisHandler implements CacheHandler {
  readonly set: (key: string, value: string) => Promise<unknown>;

  readonly getValue: (key: string) => Promise<string | null>;

  readonly deleteKey: (key: string) => Promise<number>;

  readonly findKeys: (pattern: string) => Promise<string[]>;

  readonly getValues: (specificKeys: string[]) => Promise<string[]>;

  constructor(cacheClient: RedisClient) {
    this.set = promisify(cacheClient.set).bind(cacheClient);
    this.getValue = promisify(cacheClient.get).bind(cacheClient);
    this.deleteKey = promisify(cacheClient.unlink).bind(cacheClient);
    this.findKeys = promisify(cacheClient.keys).bind(cacheClient);
    this.getValues = promisify(cacheClient.mget).bind(cacheClient);
  }
}
