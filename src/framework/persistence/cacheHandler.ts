export interface CacheHandler {
  set(key: string, value: string): Promise<unknown>;
  getValue(key: string): Promise<string | null>;
  deleteKey(key: string): Promise<number>;
  findKeys(pattern: string): Promise<string[]>;
  getValues(specificKeys: string[]): Promise<string[]>;
}
