import { APIGatewayProxyEvent } from 'aws-lambda';
import { ApiGatewayManagementApi } from 'aws-sdk';
import { PostToConnectionRequest } from 'aws-sdk/clients/apigatewaymanagementapi';
import { NonexistentEnvVarException } from '../exceptions/business';
import { LoggerScope, MessageRequest } from '../contracts';
import { Logger } from '../logger';

const functionName = 'postToConnection';

export const postToConnection = <T>(_event: APIGatewayProxyEvent, request: MessageRequest<T>) => {
  // const endpoint = `${_event.requestContext.domainName}/${_event.requestContext.stage}`;
  const endpoint = process.env.POST_TO_CONNECTION_URL;

  if (!endpoint) {
    throw new NonexistentEnvVarException("Unknown 'POST_TO_CONNECTION_URL'");
  }

  const logger = new Logger(functionName, LoggerScope.Framework);
  logger.info({ message: `The function '${functionName}' was invoked`, request });

  const apigwManagementApi = new ApiGatewayManagementApi({
    apiVersion: '2018-11-29',
    endpoint,
  });

  const params: PostToConnectionRequest = {
    ConnectionId: request.connectionId,
    Data: JSON.stringify(request.body),
  };

  return apigwManagementApi.postToConnection(params).promise();
};
