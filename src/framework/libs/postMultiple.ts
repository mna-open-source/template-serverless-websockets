import { APIGatewayProxyEvent } from 'aws-lambda';
import { LoggerScope, MessageRequest } from 'src/framework/contracts';
import { postToConnection } from 'src/framework/libs/postToConnection';
import { handleMultiplePromises } from 'src/framework/libs/handleMultiplePromises';
import { Logger } from 'src/framework/logger';

const functionName = 'postMultiple';

export const postMultiple = <T>(event: APIGatewayProxyEvent, requests: MessageRequest<T>[]): Promise<any> => {
  const logger = new Logger(functionName, LoggerScope.Framework);
  logger.info({ message: `The function '${functionName}' was invoked`, requests });

  const promises = [];

  for (const req of requests) {
    promises.push(postToConnection(event, req));
  }

  return handleMultiplePromises(promises);
};
