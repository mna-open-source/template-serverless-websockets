{
  "extends": [
    "airbnb-base",
    "plugin:jest/all",
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:@typescript-eslint/recommended",
    "plugin:prettier/recommended", // Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    "plugin:eslint-comments/recommended",
    "plugin:promise/recommended",
    "prettier" // Uses eslint-config-prettier to disable ESLint rules that would conflict with prettier
  ],
  "plugins": [
    "jest",
    "prettier",
    "@typescript-eslint",
    "prefer-arrow",
    "sort-class-members"
  ],
  "root": true,
  "globals": {},
  "rules": {
    "prettier/prettier": ["error"],
    "eslint-comments/disable-enable-pair": "off",
    "@typescript-eslint/no-unused-vars": "off",
    "@typescript-eslint/no-inferrable-types": "off",
    "@typescript-eslint/no-unused-vars-experimental": "error",
    "import/no-unresolved": [2, {"commonjs": true, "amd": true}],
    "import/prefer-default-export": "off",
    "import/no-extraneous-dependencies": ["error", {
      "devDependencies": true,
      "optionalDependencies": false,
      "peerDependencies": false
    }],
    "no-shadow": "off",
    "@typescript-eslint/no-shadow": "error",
    "import/extensions": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/explicit-module-boundary-types": "off",
    "no-restricted-syntax": "off",
    "jest/prefer-expect-assertions": "off",
    "jest/expect-expect": "off",
    "max-len": ["error", {
      "code":  150,
      "ignoreComments":  true,
      "ignoreTrailingComments":  true,
      "ignoreUrls": true,
      "ignoreStrings": true,
      "ignoreTemplateLiterals": true
    }],
    "class-methods-use-this": "off",
    "func-names": "off"
  },
  "parser": "@typescript-eslint/parser",
  "env": {
    "node": true,
    "jest": true
  },
  "overrides": [

  ],
  "settings": {
    "import/resolver": {
      "typescript": {}
    }
  }
}
