/* eslint-disable no-template-curly-in-string */

export const custom: { [k: string]: unknown } = {
  webpack: {
    webpackConfig: './webpack.config.ts',
    includeModules: true,
    keepoutputDirectory: true,
    packager: 'yarn', // Packager that will be used to package your external modules
    excludeFiles: 'src/**/*.test.[t|j]s', // Exclude test files
  },
  prune: {
    // automatically prune old lambda versions
    automatic: true,
    number: 3, // Number of versions to keep
  },
  redis: {
    port: '8888',
  },
};
