export { custom } from './serverless.custom';
export { environment } from './serverless.environment';
export { statements } from './serverless.iam';
export { packageConfig } from './serverless.package';
