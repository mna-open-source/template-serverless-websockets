export const packageConfig = {
  individually: false, // use false for faster local development
  excludeDevDependencies: true,
};
