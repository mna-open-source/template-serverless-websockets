/* eslint-disable no-template-curly-in-string */

export const environment = {
  AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
  ENV: '${self:provider.stage}',
  REGION: '${self:provider.region}',
  SLS_DEBUG: '*',
  WS_CONNECTIONS_PORT: '8888',
  WS_CONNECTIONS_HOST: '127.0.0.1',
  // WS_CONNECTIONS_PASSWORD: undefined,
  WS_CONNECTIONS_KEY_PREFIX: 'realTime:requestId:',
  POST_TO_CONNECTION_URL: 'http://localhost:3001',
};
