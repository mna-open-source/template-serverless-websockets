# Realtime service - WebSockets

## Prerequisite Software

Before you can work with this project, you must install and configure the following products on your development machine:

- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) on Mac computers, it's easier to install it using Homebrew: `brew install awscli`
- [Redis](https://redis.io/)
- [Git](http://git-scm.com) and/or the **GitHub app** (for [Mac](http://mac.github.com) or [Windows](http://windows.github.com))
- [Node.js](http://nodejs.org) - **Current version** _v12_
- [Serverless](https://www.serverless.com/framework/docs/getting-started/)
- [Yarn](https://yarnpkg.com/getting-started/install)

It is recommendable to install node via [NVM](https://github.com/nvm-sh/nvm)

## Set up your AWS Credentials

1. Log in into [Aws Console](https://mna.awsapps.com/start)
2. Expand **Aws Account** --> _Command line or programmatic access_
3. Find and copy **AWS Access Key ID** and **AWS Secret Access Key**

Now you have the information required to create a credential file:

```shell
# Run
> aws configure --profile aws-mna-profile

# Required Data
> AWS Access Key ID [None]: XXXXXXXXXXXXXXXXXXXXX
> AWS Secret Access Key [None]: XXXXXXXXXXXXXXXXXXXXX
> Default region name [None]: us-east-1
> Default output format [None]: json
```

Now validate that changes have been saved

```shell
cat ~/.aws/credentials
```

## Configure AWS Locally

Create your local profile:

```shell
# Set aws profile
> aws configure sso --profile aws-mna-profile
```

These are some configuration values:

```shell
> sso_start_url = https://mna.awsapps.com/start
> sso_region = us-east-1
> region = us-east-1
> output = json
```

## Getting the Sources

Clone this repository:

```shell
# Clone
> git clone git@gitlab.com:mna-open-source/template-serverless-websockets.git


# Go to the sources directory:
> cd template-serverless-websockets
```

## Installing NPM Modules

Next, install the JavaScript modules needed to build and test the app:

```shell
# Install project dependencies (package.json)
> yarn install
```

## Deployment

How to deploy in dev environment.

```shell
yarn aws-sso-login
yarn deploy:dev
```

## Quick Start

How to run your local environment.

```shell
serverless offline start
```

## To test communications

Open the file [websocket.html](tests/websocket.html) in the browser or follow the instruction below:

```shell
npm install wscat -g
wscat -c "ws://localhost:3001?requestId=a&otherId=b"
{"type": "test", "data": {"message": "Hi"} }
```

or 

```shell
curl --request POST --data '{"type": "test", "data": {"message": "Bye"} }' http://localhost:3000/dev/sendMessage
```
