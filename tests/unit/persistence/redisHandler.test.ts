import { RedisHandler, getConnection, CacheHandler } from '../../../src/framework/persistence';
import { setupRedisMock } from "tests/libs/setupRedisMock";

describe('set', () => {
  it('should set data', async () => {
    setupRedisMock();
    const { client } = getConnection();
    const cacheHandler: CacheHandler = new RedisHandler(client);
    const result = await cacheHandler.set('key', '1');
    expect(result).toStrictEqual('OK');
  });
});

describe('getValue', () => {
  it('should get data', async () => {
    setupRedisMock();
    const { client } = getConnection();
    const cacheHandler: CacheHandler = new RedisHandler(client);
    const result = await cacheHandler.getValue('key');
    expect(result).toStrictEqual('100');
  });
});

describe('getValues', () => {
  it('should get data', async () => {
    setupRedisMock();
    const { client } = getConnection();
    const cacheHandler: CacheHandler = new RedisHandler(client);
    const result = await cacheHandler.getValues(['key']);
    expect(result).toStrictEqual(['100']);
  });
});

describe('deleteKey', () => {
  it('should delete data', async () => {
    setupRedisMock();
    const { client } = getConnection();
    const cacheHandler: CacheHandler = new RedisHandler(client);
    const result = await cacheHandler.deleteKey('key');
    expect(result).toStrictEqual(1);
  });
});

describe('findKeys', () => {
  it('should find data', async () => {
    setupRedisMock();
    const { client } = getConnection();
    const cacheHandler: CacheHandler = new RedisHandler(client);
    const result = await cacheHandler.findKeys('key*');
    expect(result).toStrictEqual(['key']);
  });
});
