import Ajv from 'ajv';
import { disconnectionSchema } from 'src/shared/schemas/validationSchemas/disconnectionSchema';
import { onDisconnectStub } from 'tests/stubs/onDisconnect.stub';

describe('ajv onDisconnect schema validation', () => {
  it('should return true when passing a valid body', () => {
    const ajv = new Ajv({ allErrors: true, verbose: true });
    const validate = ajv.compile(disconnectionSchema);
    const response = validate(onDisconnectStub);
    expect(response).toBeTruthy();
  });
});
