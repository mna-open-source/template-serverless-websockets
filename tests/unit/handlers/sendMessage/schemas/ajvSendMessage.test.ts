import Ajv from 'ajv';
import { payloadSchema } from 'src/shared/schemas/validationSchemas/payloadSchema';
import { sendMessageStub } from 'tests/stubs/sendMessage.stub';

describe('ajv sendMessage schema validation', () => {
  it('should return true when passing a valid body', () => {
    const ajv = new Ajv({ allErrors: true, verbose: true });
    const validate = ajv.compile(payloadSchema);
    const response = validate(sendMessageStub);
    expect(response).toBeTruthy();
  });
});
