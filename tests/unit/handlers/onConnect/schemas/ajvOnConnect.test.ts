import Ajv from 'ajv';
import { connectionSchema } from 'src/shared/schemas/validationSchemas/connectionSchema';
import { onConnectStub } from 'tests/stubs/onConnect.stub';

describe('ajv onConnect schema validation', () => {
  it('should return true when passing a valid body', () => {
    const ajv = new Ajv({ allErrors: true, verbose: true });
    const validate = ajv.compile(connectionSchema);
    const response = validate(onConnectStub);
    expect(response).toBeTruthy();
  });
});
