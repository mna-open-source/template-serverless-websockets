import aws from "aws-sdk";

export const setupApiGatewayManagementApiMock = () => {
    spyOn(aws, 'ApiGatewayManagementApi').and.returnValue({
        postToConnection: (_params: any, _callback: any) => {
            return {
                promise: () => Promise.resolve(),
            };
        },
    });
};
