import redis from "redis";

export const setupRedisMock = () => {
    spyOn(redis, 'createClient').and.returnValue({
        set: (_key: any, _value: any, callback: any) => {
            callback(undefined, 'OK');
        },

        get: (_key: any, callback: any) => {
            callback(undefined, '100');
        },

        mget: (_keys: any, callback: any) => {
            callback(undefined, ['100']);
        },

        unlink: (_key: any, callback: any) => {
            callback(undefined, 1);
        },

        keys: (_pattern: any, callback: any) => {
            callback(undefined, ['key']);
        },
    });
};
