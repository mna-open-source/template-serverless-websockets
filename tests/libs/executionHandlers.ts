import { apiGatewayEventMock, contextMock } from '@schedulino/aws-lambda-test-utils';
import { APIGatewayProxyHandler, APIGatewayProxyResult } from 'aws-lambda';

export const executeSendMessage = (handler: APIGatewayProxyHandler, functionName: string, body: string | null = null): void | Promise<APIGatewayProxyResult> => {
  const context = contextMock();
  const apiGatewayEvent = apiGatewayEventMock();
  context.functionName = functionName;
  apiGatewayEvent.body = body;

  return handler(apiGatewayEvent, context, jest.fn());
};

export const executeConnection = (handler: APIGatewayProxyHandler, functionName: string, connectionId: string | undefined = undefined, queryStringParameters: { [name: string]: string | undefined; } | null = null): void | Promise<APIGatewayProxyResult> => {
  const context = contextMock();
  const apiGatewayEvent = apiGatewayEventMock();
  context.functionName = functionName;
  apiGatewayEvent.requestContext.connectionId = connectionId;
  apiGatewayEvent.queryStringParameters = queryStringParameters;

  return handler(apiGatewayEvent, context, jest.fn());
};
