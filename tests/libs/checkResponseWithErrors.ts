import { APIGatewayProxyResult } from 'aws-lambda';

export const checkResponseWithErrors = (response: void | APIGatewayProxyResult, errorCode: string, reprocessable: boolean) => {
  expect(response && response.statusCode).toStrictEqual(400);
  expect(response && typeof response.body).toBe('string');

  const body = response ? JSON.parse(response.body) : {};

  expect(body).toHaveProperty('errorCode');
  expect(body).toHaveProperty('reprocessable');
  expect(body.errorCode).toStrictEqual(errorCode);
  expect(body.reprocessable).toStrictEqual(reprocessable);
};
