import { main as def } from 'src/handlers/default/handler';
import { executeSendMessage } from 'tests/libs/executionHandlers';
import { defaultStub } from 'tests/stubs/default.stub';
import { setupRedisMock } from "tests/libs/setupRedisMock";
import { setupApiGatewayManagementApiMock } from "tests/libs/setupApiGatewayManagementApiMock";
import { checkResponseWithErrors } from "tests/libs/checkResponseWithErrors";

jest.mock('aws-sdk');

describe('default function', () => {
  it('should be define', () => {
    expect(def).toBeDefined();
  });

  it('should be a function', () => {
    expect(typeof def).toBe('function');
  });

  it('when the event body is valid, it will return a successful response.', async () => {
    setupRedisMock();
    setupApiGatewayManagementApiMock();

    const body = JSON.stringify(defaultStub);
    const response = await executeSendMessage(def, 'default', body);

    expect(response && response.statusCode).toStrictEqual(200);
    expect(response && typeof response?.body).toBe('string');
  });

  it('when the event body is null, it will return a response with error.', async () => {
    setupRedisMock();
    setupApiGatewayManagementApiMock();

    const response = await executeSendMessage(def, 'default', null);

    checkResponseWithErrors(response, '1', false);
  });

  it('when the event body is not JSON, it will return a response with error.', async () => {
    setupRedisMock();
    setupApiGatewayManagementApiMock();

    const response = await executeSendMessage(def, 'default', 'ssss');

    checkResponseWithErrors(response, '3', false);
  });

  it('when the event body is not expected, it will return a response with error.', async () => {
    setupRedisMock();
    setupApiGatewayManagementApiMock();

    const body = JSON.stringify({ dummy: 'Matias' });
    const response = await executeSendMessage(def, 'default', body);

    checkResponseWithErrors(response, '3', false);
  });
});
