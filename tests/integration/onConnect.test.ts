import { main as onConnect } from 'src/handlers/onConnect/handler';
import { executeConnection } from 'tests/libs/executionHandlers';
import { setupRedisMock } from "tests/libs/setupRedisMock";
import { checkResponseWithErrors } from "tests/libs/checkResponseWithErrors";
import { onConnectStub } from "tests/stubs/onConnect.stub";

describe('onConnect function', () => {
    it('should be define', () => {
        expect(onConnect).toBeDefined();
    });

    it('should be a function', () => {
        expect(typeof onConnect).toBe('function');
    });

    it('when the event connection id with payload are valid, it will return a successful response.', async () => {
        setupRedisMock();

        const response = await executeConnection(onConnect, 'onConnect', onConnectStub.id, onConnectStub.payload);

        expect(response && response.statusCode).toStrictEqual(200);
        expect(response && typeof response?.body).toBe('string');
    });

    it('when the event connection id is null, it will return a response with error.', async () => {
        setupRedisMock();

        const response = await executeConnection(onConnect, 'onConnect');

        checkResponseWithErrors(response, '3', false);
    });

    it('when the event connection payload is null, it will return a response with error.', async () => {
        setupRedisMock();

        const response = await executeConnection(onConnect, 'onConnect', onConnectStub.id);

        checkResponseWithErrors(response, '3', false);
    });
});

