import { main as sendMessage } from 'src/handlers/sendMessage/handler';
import { executeSendMessage } from 'tests/libs/executionHandlers';
import { sendMessageStub } from 'tests/stubs/sendMessage.stub';
import { setupRedisMock } from "tests/libs/setupRedisMock";
import { setupApiGatewayManagementApiMock } from "tests/libs/setupApiGatewayManagementApiMock";
import { checkResponseWithErrors } from "tests/libs/checkResponseWithErrors";

jest.mock('aws-sdk');

describe('sendMessage function', () => {
    it('should be define', () => {
        expect(sendMessage).toBeDefined();
    });

    it('should be a function', () => {
        expect(typeof sendMessage).toBe('function');
    });

    it('when the event body is valid, it will return a successful response.', async () => {
        setupRedisMock();
        setupApiGatewayManagementApiMock();

        const body = JSON.stringify(sendMessageStub);
        const response = await executeSendMessage(sendMessage, 'sendMessage', body);

        expect(response && response.statusCode).toStrictEqual(200);
        expect(response && typeof response?.body).toBe('string');
    });

    it('when the event body is null, it will return a response with error.', async () => {
        setupRedisMock();
        setupApiGatewayManagementApiMock();

        const response = await executeSendMessage(sendMessage, 'sendMessage', null);

        checkResponseWithErrors(response, '1', false);
    });

    it('when the event body is not JSON, it will return a response with error.', async () => {
        setupRedisMock();
        setupApiGatewayManagementApiMock();

        const response = await executeSendMessage(sendMessage, 'sendMessage', 'ssss');

        checkResponseWithErrors(response, '3', false);
    });

    it('when the event body is not expected, it will return a response with error.', async () => {
        setupRedisMock();
        setupApiGatewayManagementApiMock();

        const body = JSON.stringify({ dummy: 'Matias' });
        const response = await executeSendMessage(sendMessage, 'sendMessage', body);

        checkResponseWithErrors(response, '3', false);
    });
});
