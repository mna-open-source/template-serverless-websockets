import { main as onDisconnect } from 'src/handlers/onDisconnect/handler';
import { executeConnection } from 'tests/libs/executionHandlers';
import { setupRedisMock } from "tests/libs/setupRedisMock";
import { checkResponseWithErrors } from "tests/libs/checkResponseWithErrors";
import { onDisconnectStub } from "tests/stubs/onDisconnect.stub";

describe('onDisconnect function', () => {
    it('should be define', () => {
        expect(onDisconnect).toBeDefined();
    });

    it('should be a function', () => {
        expect(typeof onDisconnect).toBe('function');
    });

    it('when the event connection id is valid, it will return a successful response.', async () => {
        setupRedisMock();

        const response = await executeConnection(onDisconnect, 'onDisconnect', onDisconnectStub.id, onDisconnectStub.payload);

        expect(response && response.statusCode).toStrictEqual(200);
        expect(response && typeof response?.body).toBe('string');
    });

    it('when the event connection id is null, it will return a response with error.', async () => {
        setupRedisMock();

        const response = await executeConnection(onDisconnect, 'onDisconnect');

        checkResponseWithErrors(response, '3', false);
    });
});

