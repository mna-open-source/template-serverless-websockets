process.env.WS_CONNECTIONS_PORT = '8888';
process.env.WS_CONNECTIONS_HOST = '127.0.0.1';
process.env.WS_CONNECTIONS_KEY_PREFIX = 'realTime:requestId:';
process.env.POST_TO_CONNECTION_URL = 'http://localhost:3001';
