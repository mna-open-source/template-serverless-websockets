import { Payload } from 'src/shared/entities/payload';

export const sendMessageStub: Payload = {
  type: 'test',
  data: {
    message: 'Hi',
  },
};
