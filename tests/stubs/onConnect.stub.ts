import { CustomConnectionData } from 'src/shared/entities/customConnectionData';

export const onConnectStub: CustomConnectionData = {
    id: 'c#',
    payload: {
        requestId: 'r#',
        otherId: 'o#',
    },
};
