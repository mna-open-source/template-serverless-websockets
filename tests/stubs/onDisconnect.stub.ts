import { DisconnectionData } from 'src/shared/entities/disconnectionData';

export const onDisconnectStub: DisconnectionData = {
    id: 'c#',
    payload: null,
};
