import { Payload } from 'src/shared/entities/payload';

export const defaultStub: Payload = {
  type: 'test',
  data: {
    message: 'Hi',
  },
};
